from ipaddress import IPv4Network
import json

while True:
    try:
        input_address = (IPv4Network(input("Address/Netmask :"), False))
        break
    except ValueError:
        print("Please, enter valid value!")


class Ip:
    def __init__(self, ip):
        self.values = {}
        self.ip = ip

    def calculate(self):
        self.values["Network Address"] = self.ip.network_address
        self.values["Netmask Address"] = self.ip.netmask
        self.values["Broadcast Address"] = self.ip.broadcast_address
        self.values["Hostmask Address"] = self.ip.hostmask
        self.values["Total Number Of Address"] = self.ip.num_addresses
        if self.ip.num_addresses < 3:
            self.values["First Address"] = self.ip.network_address
            self.values["Last Address"] = self.ip.broadcast_address
        else:
            self.values["First Address"] = self.ip[1]
            self.values["Last Address"] = self.ip.broadcast_address - 1

        return self

    def print(self):
        for each in self.values:
            print(each + ": " + str(self.values[each]))

    def to_json(self):
        temp = {}
        for each in self.values:
            temp[str(each).lower().replace(" ", "_")] = str(self.values[each])

        temp = json.dumps(temp)
        print(temp)


Ip(input_address).calculate().print()
